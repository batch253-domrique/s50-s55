import { Button, Row, Col } from 'react-bootstrap';
// import { useState } from 'react';
// import { Container, Navbar, Nav } from 'react-bootstrap';
// import { Link, NavLink } from 'react-router-dom';

export default function Banner() {
    return (
        <Row>
            <Col className="p-5">
                <h1>Zuitt Coding Bootcamp</h1>
                <p>Opportunities for everyone, everywhere.</p>
                <Button variant="primary">Enroll Now!</Button>
            </Col>
        </Row>
    );
}

// export default function Banner() {
//     const [endpoint, setEndpoint] = useState();
//     return (
//         <Row>
//             {endpoint === '*' ? (
//                 <Col>
//                     <h1>Page Not Found</h1>
//                     <p>
//                         Go back to the <Link to="/">homepage</Link> .
//                     </p>
//                 </Col>
//             ) : (
//                 <>
//                     <Col>
//                         <h1>Zuitt Coding Bootcamp</h1>
//                         <p>Opportunities for everyone, everywhere.</p>
//                         <Button variant="primary">Enroll Now!</Button>
//                     </Col>
//                 </>
//             )}
//         </Row>
//     );
// }
