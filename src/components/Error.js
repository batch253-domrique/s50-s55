import { Button, Row, Col } from 'react-bootstrap';
// import { useState } from 'react';
// import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export default function Error() {
    return (
        <Row>
            <Col>
                <h1>Page Not Found</h1>
                <p>
                    Go back to the <Link to="/">homepage</Link> .
                </p>
            </Col>
        </Row>
    );
}
